# Demo of https://github.com/mdgriffith/elm-optimize-level-2/issues/93

This repository demonstrates the bug described in the issue.

## Steps to reproduce

1. Clone the repository. 

1. Make sure you have Elm and Node.js installed.

1. Run `npm clean-install` to get elm-optimize-level-2.

1. Run `npx elm-optimize-level-2 src/Main.elm`

   We have the following output:

   ```
   Compiled, optimizing JS...
   Error: something went wrong, expected number of arguments=256 but got 0 for step
   ```

1. Change the import in the `src/Main.elm` from `SHA256` to `SHA224`

   Or simply apply this diff:
   
   ``` diff
   diff --git a/src/Main.elm b/src/Main.elm
   index 4229028..5adfc7f 100644
   --- a/src/Main.elm
   +++ b/src/Main.elm
   @@ -1,12 +1,12 @@
    module Main exposing (main)
   
    import Html exposing (Html)
   -import SHA256
   +import SHA224
   
   
    main : Html msg
    main =
        "Hello, buggy!"
   -        |> SHA256.fromString
   +        |> SHA224.fromString
            |> always "It worked"
            |> Html.text
   ```

1. Run `npx elm-optimize-level-2 src/Main.elm` again

   Interestingly using SHA224 module instead of SHA256 resulted in the following:

   ```
   Compiled, optimizing JS...
   Error: something went wrong, expected number of arguments=224 but got 6 for a
   ```

1. Notice the number of arguments in the error message.

   It's consistent with the number after the package name!

1. Remove the call to `SHA256.fromString` 

   ```diff
   diff --git a/src/Main.elm b/src/Main.elm
   index 4229028..16519c1 100644
   --- a/src/Main.elm
   +++ b/src/Main.elm
   @@ -7,6 +7,6 @@ import SHA256
    main : Html msg
    main =
        "Hello, buggy!"
   -        |> SHA256.fromString
   +        -- |> SHA256.fromString
            |> always "It worked"
            |> Html.text
   ```
1. See it working without any errors

   ```
   $ npx elm-optimize-level-2 src/Main.elm
   Compiled, optimizing JS...
   Success!
   
      Main.elm ───> elm.js
   
   ```
