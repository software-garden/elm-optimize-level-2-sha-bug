{
  description = "Demo of https://github.com/mdgriffith/elm-optimize-level-2/issues/93";

  inputs = {
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
    flake-utils = {
      url = "github:numtide/flake-utils";
    };
  };

  outputs = { self, nixpkgs, flake-utils, flake-compat }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        shared-inputs = [
            pkgs.elmPackages.elm
            pkgs.nodejs
        ];
      in rec {
        defaultPackage = pkgs.stdenv.mkDerivation {
          name = "elm-optimize-level-2-sha-bug";
          src = self;
          buildInputs = shared-inputs ++ [
          ];
        };

        devShell = pkgs.mkShell {
          name = "elm-optimize-level-2-sha-bug-development-shell";
          src = self;
          buildInputs = shared-inputs ++ [
          ];
        };

      }
    );
}
