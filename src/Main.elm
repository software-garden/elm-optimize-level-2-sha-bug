module Main exposing (main)

import Html exposing (Html)
import SHA256


main : Html msg
main =
    "Hello, buggy!"
        |> SHA256.fromString
        |> always "It worked"
        |> Html.text
